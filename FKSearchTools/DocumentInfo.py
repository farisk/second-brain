#Represents the details of a term in a document, e.g. how many times is it present?,
#Does it occur in the title?
class DocumentInfo:

	def __init__(self,docID):
		self.appearsInTitle = False
		self.docID = docID
		self.termFrequncies = {} #A dictionary storing the frequncy of terms within it
		self.count = 0

	def incrementTermFrequncy(self,term):
		if term in self.termFrequncies:
			termFrequncies[term] += 1
		else:
			termFrequncies[term] = 1

	def incrementCount(self):
		self.count += 1

	def setTitleTrue(self):
		self.appearsInTitle = True
