
import termManipulation
import math
import infoRetrieval as ir
import similarityMetrics
from DocumentInfo import DocumentInfo

class InvertedIndex:


	def __init__(self,useStopList,chunks):
		self.invertedIndex = {} #Key is a string(the term) and value is a list of relevent DocumentInfo class
		self.termFrequncies = {} #holds the frequncey of a term appearing in documents in the collection
		self.totalDocs = len(chunks) #Total  number of chunks in collection
		if useStopList:
			self.stopList = getStopList('FKSearchTools/stopwords.txt')
		else:
			self.stopList = []

		for chunk in chunks:
			self.processChunk(chunk)
		#Calculate inverse document frequncies
		self.inverseDocumentFrequncies = self.calculateIDFs()
		#self.chunks = chunks

	def getDocumentInfo(self,term,docID):
		if term in self.invertedIndex:
			docInfos = self.invertedIndex[term]
			for doc in docInfos:
				if doc.docID == docID:
					return doc
			#If we are still in the function the relevant docInfo was not found so create then return it
			newDocInfo = DocumentInfo(docID)
			docInfos.append(newDocInfo) #update inverted index with new doc info object
			return newDocInfo
		else: #Term doesnt exist in the invertedIndex so create it
			newDocInfo = DocumentInfo(docID)
			self.invertedIndex[term] = [newDocInfo]#Create list with new doc info object 
			return newDocInfo

	#Returns the frequncy of a term in a document from inverted index 
	def getTermFrequncy(self,term,docID):
		if term in self.invertedInex:
			docInfos = self.invertedIndex[term]
		else:
			return 0

	#Call when we encounter a term that hasnt been seen before in a document to keep track
	#of term frequncy in collection as a whole
	def incrementTermFrequency(self,term):
		if term in self.termFrequncies:
			self.termFrequncies[term] += 1
		else:
			self.termFrequncies[term] = 1

	def processChunk(self,chunk):
		#Process chunk title
		titleList = chunk.name.split()
		contentList = chunk.content.split()

		titleTerms = self.manipulateTermList(titleList) #List of words in title, terms processed
		contentTerms = self.manipulateTermList(contentList) #List of words in content, terms processed 

		termsSeen = []#contains terms we have dealt with, store to avoid double counting term frequency in collection
		#For every term in the title and the content update the inverted index counts
		for term in titleTerms:
			docInfo = self.getDocumentInfo(term,chunk.id)
			docInfo.incrementCount()#increment term frequncy count
			docInfo.setTitleTrue()#This term appears in the chunks title, store that information(useful for weighting schemes)
			if not(term in termsSeen):
				self.incrementTermFrequency(term)
				termsSeen.append(term)

		for term in contentTerms:
			docInfo = self.getDocumentInfo(term,chunk.id)
			docInfo.incrementCount()
			if not(term in termsSeen):
				self.incrementTermFrequency(term)
				termsSeen.append(term)


	def manipulateTermList(self,terms):
		stopWordsRemoved = termManipulation.removeStopWords(self.stopList,terms)

		return map(manipulateTerm, stopWordsRemoved) #Apply remaining term manipulations on terms not in stop list and return


	#use the term frequncy dictionary to calculate inverse document frequncies, return dictionary of IDFS
	def calculateIDFs(self):
		print 'calculating inverse document frequencies...'
		idfs = {}
		for term,count in self.termFrequncies.iteritems():
			idfs[term] = math.log10(self.totalDocs/count)
		return idfs

	def termWeighting(self,term,appearsInTitle = False):
		return self.inverseDocumentFrequncies[term]#return weight * idf 

	def retrieveRelevantDocs(self,query):
		return ir.calculateDocumentSimilarities(self,query,similarityMetrics.cosineSimularity)


#manipulate individual terms so like terms are grouped together
def manipulateTerm(term):
	#Set term to lowercase and remove punctuation 	
	return termManipulation.capitalisation(termManipulation.removePunctuation(term))


def getStopList(fileName):
	stopWordFile = open(fileName,'r')
	stopWordList = []
	for word in stopWordFile:
		stopWordList.append(word.replace("\n",""))

	return stopWordList