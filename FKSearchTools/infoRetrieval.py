#Calculate the similarties of all documents which contain at least one term from the query
def calculateDocumentSimilarities(InvertedIndex,query,similarityFunction):
	similartyScores = {} #DOCID as keys, Similarty score as value. To be returned 
	queryTerms = query.split()
	queryTerms = InvertedIndex.manipulateTermList(queryTerms)#apply same manipulations to query as we did with the documents

	queryTermCounts = {} #stores frequncy of terms in query
	uniqueQueryTerms = []#list of unique terms in query
	queryVector = [] #this will contain the vector representation of the query
	for i in range(0,len(queryTerms)):
		term = queryTerms[i]
		if term in queryTermCounts:
			queryTermCounts[term] += 1
		else:
			queryTermCounts[term] = 1
			uniqueQueryTerms.append(term)

	queryLength = 0#Used to produce document vectors of appropriate lengths

	#Calculate the number of dimensions the vectors will be
	for term in InvertedIndex.invertedIndex:
		if term in uniqueQueryTerms:
			queryLength += 1


	similarDocuments = {} #Contains vectors of documents that have a non zero similarity
	termNumber = 0#Count of how many query terms processed so far, used so we can update appropraite vector positinos

	for queryTerm in uniqueQueryTerms:
		if queryTerm in InvertedIndex.invertedIndex:
			queryVector.append(queryTermCounts[queryTerm] * InvertedIndex.termWeighting(queryTerm))#Form query vector and apply weighting
			docInfos = InvertedIndex.invertedIndex[queryTerm]
			for docInfo in docInfos:#for all documents containing the term
				if docInfo.docID in similarDocuments:
					#Document in question seen before so update it's vector
					similarDocuments[docInfo.docID][termNumber] = docInfo.count#set terms frequncy
				else:
					#Document has not seen before create a vector of appropriate length for it
					docVector = [0]*queryLength
					docVector[termNumber] = docInfo.count
					similarDocuments[docInfo.docID] = docVector
				#Apply weightings to vector
				similarDocuments[docInfo.docID][termNumber] *=  InvertedIndex.termWeighting(queryTerm,docInfo.appearsInTitle)

		termNumber = termNumber + 1

	for docID,docVector in similarDocuments.iteritems():
		similartyScores[docID] = similarityFunction(queryVector,docVector)

	return similartyScores


#Given a dictionary return a list of keys sorted by their values in descending order
def sortResults(similarityDict):
	return sorted(similarityDict.iterkeys(), key=lambda k: similarityDict[k],reverse=True)

