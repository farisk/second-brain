import infoRetrieval as ir
#Returns the N(numberOfDocsToReturn) most similar documents to centerChunk
def getSimilarDocuments(invertedIndex,centerChunk,numberOfDocsToReturn):
		#Just do a search to find documents with similar terms to given document
		searchResults =  invertedIndex.retrieveRelevantDocs(str(centerChunk.name + " " + centerChunk.content))
		sortedResults = ir.sortResults(searchResults)

		#We do not want to return the center chunk in the similarity list
		#if centerChunk.id in sortedResults:
			#sortedResults.remove(centerChunk.id)

		if len(sortedResults) < numberOfDocsToReturn:
			return sortedResults
		else:
			return sortedResults[0:numberOfDocsToReturn]