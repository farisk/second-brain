import math
#return A.B/|A| x |B|
def cosineSimularity(vector1,vector2):
	if not(len(vector1) == len(vector2)):
		raise ValueError("The vectors provided do not have the same dimensions")  
	else:
		dotProduct = 0 #will contain dot product of vector 1 and vector 2
		sumOfSquaresV1 = 0
		sumOfSquaresV2 = 0
		for i in range(0,len(vector1)):
			dotProduct += vector1[i] * vector2[i]
			sumOfSquaresV1 += math.pow(vector1[i],2) 
			sumOfSquaresV2 += math.pow(vector2[i],2)

		V1Length = math.sqrt(sumOfSquaresV1)
		V2Length = math.sqrt(sumOfSquaresV2)
		return dotProduct/(V1Length*V2Length)