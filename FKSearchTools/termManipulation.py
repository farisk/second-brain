import string
#TODO: Lemmatisation,Stemming,Normalisation



def capitalisation(word):
	return word.lower()

#tokenisation
def removePunctuation(word):
	return word.translate(string.maketrans("",""), string.punctuation)



#given a list of terms remove the words in the stopWord list
def removeStopWords(stopWords,termList):
	for stopWord in stopWords:
		#Remove all instances of the stopWord in the given term list
		while stopWord in termList:
			termList.remove(stopWord)
	return termList

def removeStopWords_backup(stopList,terms):
	print terms
	ret = terms
	for stopWord in stopList:
		ret = ret.replace(stopWord,"")

	return ret #Return string with stopWords removed
