class Chunk:

	def __init__(self,idCode):
		self.subjects = []
		self.children = []
		self.id = idCode

		self.name = "N/A"
		self.date = '01/01/2000'
		self.content = 'N/A'

	def setName(self,name):
		self.name = name
	def setParentChunk(self,chunk):
		self.parent = chunk
	def addSubject(self,subject):
		self.subjects.append(subject)
	def setDate(self,date):
		self.date = date
	def addChildChunk(self,chunk):
		self.children.append(chunk)
	def setContent(self,content):
		self.content = content


	def toJSON(self):
		return {'id' : self.id, 'name': self.name,'date': str(self.date), 'content': self.content, 'children' : map(lambda c:(c.id,c.name),self.children)}

	#return toJSON without content
	def summaryJSON(self):
		return {'id' : self.id, 'name': self.name,'date': str(self.date), 'subjects': self.subjects, 'children' : map(lambda c:(c.id,c.name),self.children)}
