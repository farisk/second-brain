####################
#Author:Faris Keenan
#Date Created:04/01/2015
####################
from xml.etree.ElementTree import iterparse
from chunk import Chunk
import time
import sys
def normaliseSubject(word):
	return word.lower()

def updateSubjectDict(subjectDict,chunk,subject):
	if subject in subjectDict:
		subjectDict[subject].append(chunk)
	else:
		#Subject hasnt been encountered in the dictionary before so add it
		subjectDict[subject] = [chunk]

def processXMLBrain(fileName):
	processingChunk = False
	globalChunkList = {}#containts all chunk nodes
	rootChunks = {}#contains only chunks nodes with no ancestors(roots)
	subjectDict = {} #Dictionary of subjects, index is subject value is a list of chunks which are under the subject
	for (event,node) in iterparse(fileName, ['start','end']):

		if event == 'end':#Reached an end tag
			if node.tag == 'chunk':
				if currentChunk.id in globalChunkList.keys():
					print 'Error: Chunk with ID ' , currentChunk.id , ' already exists!'
					sys.exit()
				globalChunkList[currentChunk.id] = currentChunk#add chunk to global chunk list
				if not hasattr(currentChunk, 'parent'):
					#The chunk we were dealing with has no children, it is completed.
					processingChunk = False
					rootChunks[currentChunk.id] = currentChunk#Add chunk to root chunk list
				else:
					#We have finished processing a child chunk, return to processing its parent
					currentChunk.parent.addChildChunk(currentChunk)#Add the chunk to its parents child list
					currentChunk = currentChunk.parent #return to parent processing
		elif event =='start':#Reached a start tag
			if node.tag == 'chunk':
				if not processingChunk:
					currentChunk = Chunk(node.attrib.get('id'))
					processingChunk = True
					print "New root chunk found id:" , currentChunk.id
				else:
					#We have encountered a child chunk!
					newChunk = Chunk(node.attrib.get('id'))
					#print "New child chunk found id:" , currentChunk.id
					newChunk.setParentChunk(currentChunk)#Set parent chunk so we remember what chunk to continue processing when we finish with this child(newChunk)
					currentChunk = newChunk
			if processingChunk: #Safety check, only process chunk attributes if one has actually been started
				if node.tag == 'date':
					currentChunk.setDate(time.strptime(node.text, "%d/%m/%Y"))
				elif node.tag == 'subject':
					normalisedSub = normaliseSubject(node.text)
					currentChunk.addSubject(normalisedSub)
					updateSubjectDict(subjectDict,currentChunk,normalisedSub)#add chunk to subject list in dictionary
				elif node.tag == 'content':
					if node.text:
						content =  node.text.replace('\n',"</br>")#replace newline character with breaktag for display in html
						currentChunk.setContent(str(content))
				elif node.tag == 'name':
					currentChunk.setName(str(node.text))

	return (globalChunkList,rootChunks,subjectDict)