from bottle import route,run,template,view,static_file,request
from core import chunkLoader
from FKSearchTools.indexBuilder import InvertedIndex
import FKSearchTools.similarDocsExtractor as similarDocsExtractor

import webHelper as whf #web helper functions
import FKSearchTools.infoRetrieval as ir
import mindMap.mindMap as mindMap
ip = 'localhost'
port = '8080'
fileName = 'mind.xml'
print 'loading brain from ' , fileName

brain = chunkLoader.processXMLBrain(fileName)
subjectDict = brain[2]
globalChunkDict = brain[0]
rootChunkDict = brain[1]
print 'Total Root Chunks:', len(rootChunkDict)
print 'Total Chunks:', len(globalChunkDict)
#Build the inverted index for searching
print "Building inverted index...."
invertedIndex = InvertedIndex(True,globalChunkDict.values())


#Create static menu bar
nav = '<ul>'
nav += "<li>" + whf.toLink('/','Home') + "</li>"
nav += "<li>" + whf.toLink('/viewAllChunks','View Chunks') + "</li>"
nav += "<li>" + whf.toLink('/search','Search Chunks') + "</li>"
nav += "</ul>"
mb = '<div id="menu">' + nav + "</div>"
def menuBar():
	return mb

#Adds values for wildcards that are present in old templates, e.g. Menu Bar
def addDefaultValues(dict):
	dict['menu'] =  menuBar()
	return dict
	
@route('/')
@view('index')
def index():
	var = {'xmlFileName':fileName,
			'totalChunks':len(globalChunkDict),
			'menu' : menuBar()}
	return var

@route('/viewAllChunks')
@view('chunkList')
def viewAllChunks():
	subject =  request.query.subject

	if subject in subjectDict:
		items = whf.listAllChunks(subjectDict[subject])
	else:
		items = whf.listAllChunks(rootChunkDict.values())
	var = {'chunks' : items }
	return addDefaultValues(var)
	
@route('/viewChunks')
def viewChunks():
	subject =  request.query.subject

	items = {};
	if subject in subjectDict:
		items = (subjectDict[subject])
	else:
		items = (rootChunkDict.values())

	return {'chunks':map(lambda chunk:chunk.summaryJSON(),items)};

@route('/search')
@view('search')
def search():
	searchQuery = request.query.query
	var = {}
	if not(len(searchQuery) > 0):
		#empty values as no query has been made
		var['results'] = ""
		var['numberOfResults'] = ""
	else:
		#Do a search using query
		searchResults =  invertedIndex.retrieveRelevantDocs(str(searchQuery))
		sortedResults = ir.sortResults(searchResults)
		chunkResults = []
		#Form a list of chunks in order of relevancy
		for result in sortedResults:
			chunkResults.append(globalChunkDict[result])
		var['numberOfResults'] = str(len(chunkResults)) + " results returned."
		var['results'] = whf.displayResults(chunkResults)
		#var['results'] = whf.listAllChunks(chunkResults)

	return addDefaultValues(var)



@route('/open/<chunkid>')
def openChunk(chunkid):

	chunk = {'error':"Chunk with id " + chunkid + " does not exist"}
	if chunkid in globalChunkDict:
		chunk = globalChunkDict[chunkid].toJSON()
	#return addDefaultValues(var)
	return chunk

@route('/chunkGraph/<chunkid>/<depth>/<breadth>')
def chunkGraph(chunkid,depth,breadth):
	mindmap = mindMap.buildMindMap(invertedIndex, globalChunkDict[chunkid], int(depth), int(breadth),globalChunkDict)

	return mindmap.toJSON()
@route('/invertedIndex')
def showInvertedIndex():
	html = "<h1>Inverted Index</h1>"
	for term,docList in invertedIndex.invertedIndex.iteritems():
		html += "<p>"
		html += "<b>" + term +"</b> </br>"
		for doc in docList:
			html += "Document:" + doc.docID + " Occurances:" + str(doc.count)+ " In Title:" + str(doc.appearsInTitle) + "</br>"
		html += "</p>"
	return html

@route('/static/:path#.+#', name='static')
def static(path):
	return static_file(path, root='static')
run(host=ip, port=port, debug=True)#starts built in development server
