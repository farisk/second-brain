import FKSearchTools.similarDocsExtractor as similarDocsExtractor
#MindMap is an undirected graph
class MindMap:

	def __init__(self):
		self.nodes = set()#Set contains vertices
		self.edges = []#List of sets. Unidrectional graph so order of edge doesnt matter.

	def addNode(self,node):
		self.nodes.add(node)

	#given a set which represents an edge(list of two nodes)
	def addEdge(self,edgeSet):
		if not(edgeSet in self.edges):
			self.edges.append(edgeSet)

	def toJSON(self):
		return {'vertices':map(lambda y: {'id' : y.id, 'label' : y.name },self.nodes),'edges' : map(lambda x:list(x),self.edges)}

def populateMindMap(invertedIndex,centerDocument,depth,breadth,mindMap,globalChunkDict):
	if depth == 0:#Base case stop recursing
		return
	else:
		similarDocs = similarDocsExtractor.getSimilarDocuments(invertedIndex, globalChunkDict[centerDocument], breadth)
		for doc in similarDocs:
			if not(doc in mindMap.nodes):
				mindMap.addNode(globalChunkDict[doc])#Add new node to mind map(if it doesn't exist ofcourse)
				mindMap.addEdge(set([centerDocument,doc]))#add the edge if it doesn't exist
				populateMindMap(invertedIndex,doc,depth-1,breadth,mindMap,globalChunkDict)#Recurse and find relevant nodes for this doc
	return

def buildMindMap(invertedIndex,centerDocument,depth,breadth,globalChunkDict):
	mindMap = MindMap()
	print centerDocument.name
	populateMindMap(invertedIndex,centerDocument.id,depth,breadth,mindMap,globalChunkDict)
	print len(mindMap.nodes)
	return mindMap