var app = angular.module("myBrain",['ngRoute']);

app.config(['$routeProvider',function($routeProvider)
{
	$routeProvider.when('/',{
		template:'<p>Welcome to your second brain</p>'
	}).when('/chunks',{
		template:'<chunks></chunks>'
	}).when('/openchunk/:chunkid',{
		template:'<chunk></chunk>'
	}).when('/viewMindMap/:chunkid',{
		templateUrl:'viewMindMap.html',
		controller:"AppCtrl"
	}).otherwise({
		redirectTo: '/'
	});
}]);

app.controller("AppCtrl", function ($scope, $routeParams) {
  $scope.chunkid=  $routeParams.chunkid;
  getMindMap($scope.chunkid);//Call function from mindmap.js
});

app.directive('chunk',function()
{
	return {
		restrict:'E',
		controller:['$http','$routeParams',function($http,$routeParams){
				var self = this;
				self.chunkid = $routeParams.chunkid;
				$http.get("/open/" + self.chunkid).success(function(data){ 
					self.chunk = data;
					console.log(data);
				});
}],
		controllerAs:"chunk",
		templateUrl:"chunk.html"
	}
});

app.directive('chunks', function()
{
	return {
		restrict :'E',
		controller:['$http',function($http){
				var self = this;
				self.chunks = [];

				$http.get("/viewChunks").success(function(data){ 
					self.chunks = data.chunks;
					console.log(data);
				});
}],
		controllerAs:"ChunkSummaries",
		templateUrl:'chunkSummaries.html',

	};
});



