from core import chunkLoader



fileName = 'mind.xml'

print 'loading brain from ' , fileName
running = True
brain = chunkLoader.processXMLBrain(fileName)
subjectDict = brain[2]
globalChunkDict = brain[0]
rootChunkDict = brain[1]

print 'Brain loaded'
print 'Total number of chunks:', len(globalChunkDict)
print 'Number of root chunks:', len(rootChunkDict)
print 'Number of subjects:', len(subjectDict)




def listAllChunks():
	for idCode,chunk in rootChunkDict.iteritems():
		displayChunkSummary(chunk)

def askOpen():
	idCode = (raw_input("Enter Chunk ID:"))
	if idCode in globalChunkDict:
		showChunk(globalChunkDict[idCode])
	else:
		print 'No chunk with that ID exists'

def showChunk(chunk):
	whitespace = ''
	print '-'*40
	print whitespace, 'Name:', chunk.name
	print whitespace, 'id:' , chunk.id
	print whitespace, 'Date:' , chunk.date[2] ,'-', chunk.date[1], '-' ,chunk.date[0]
	print whitespace, 'Subjects:', chunk.subjects

	childrenInfo ='Children:'
	if len(chunk.children) > 0:
		for child in chunk.children:
			childrenInfo += '(' + child.id + ',' + child.name + ')'
	else:
		childrenInfo =  'Chunk has no children'
	print childrenInfo

	print '-'*5 , 'Content' , '-'*5
	print chunk.content


def displayChunkSummary(chunk,level = 0):
	whitespace = ' ' * (level*4)
	print whitespace, '----------------------------'
	print whitespace, 'Name:', chunk.name
	print whitespace, 'id:' , chunk.id
	print whitespace, 'Date:' , chunk.date[2] ,'-', chunk.date[1], '-' ,chunk.date[0]
	print whitespace, 'Subjects:', chunk.subjects

	for child in chunk.children:
		displayChunkSummary(child,level+1)

def listSubjects():
	for subject in subjectDict:
		print subject

def listChunksWithSubject():
	subject = raw_input("Enter subject:")
	if subject in subjectDict:
		for chunk in subjectDict[subject]:
			showChunk(chunk)
	else:
		print 'No chunks with that subject'
def exit():
	global running
	running = False


def listCommands():
	for key in controls.keys():
		print key

controls = {'list all': listAllChunks,
			'open' : askOpen,
			'exit' : exit,
			'list subjects' : listSubjects,
			'list subject' : listChunksWithSubject,
			'list commands' : listCommands}




while running == True:
	print '\n'
	command = raw_input("Command:")
	if command in controls:
		controls[command]()#run the function related to the command
	else:
		print 'Command does not exist! Use list commands to see commands'

