def toLink(url,text):
	return "<a href=\"" + url + "\">" + text + "</a>"

def showChunk(chunk):
	whitespace = '<p>'
	retString = whitespace + '<h1>' + chunk.name + '(' + chunk.id + ')</h1>'
	retString += '<a href=/static/mindmap.html>View mind map</a>'
	retString += whitespace + 'Date:' + str(chunk.date[2]) + '-' + str(chunk.date[1]) + '-' + str(chunk.date[0]) + '</p>'
	retString += whitespace + 'Subjects:' + presentSubjectList(chunk.subjects)  + '</p>'

	if hasattr(chunk,'parent'):
		retString+='<p>Parent: <a href="/open/' +  chunk.parent.id +  '">' + chunk.parent.name + '</a></p>'
	childrenInfo ='Children:'
	if len(chunk.children) > 0:
		for child in chunk.children:
			childrenInfo += '<a href=\"/open/' + child.id + "\">" + child.name + "</a>, "

	else:
		childrenInfo =  'Chunk has no children'

	retString += whitespace + childrenInfo + "</p>"
	retString += whitespace + chunk.content + "</p>"

	return retString


def displayChunkSummary(chunk,level = 0):
	retString =''
	whitespace = '<p>'
	retString += '<h2><a href="/open/' + chunk.id + '">' + chunk.name + '(' + chunk.id + ')</a></h2>'
	retString += whitespace + 'Date:' + str(chunk.date[2]) +'-'+ str(chunk.date[1]) + '-' + str(chunk.date[0]) + '</p>'
	retString += whitespace + 'Subjects:' + presentSubjectList(chunk.subjects) + '</p>'

	for child in chunk.children:
		retString += '<div id="childSummary">' + displayChunkSummary(child,level+1) + '</div>'

	return retString


def presentSubjectList(subjects):
	ret =''
	for subject in subjects:
		ret += toLink('/viewAllChunks?subject=' + subject,subject) + ","
	return ret


def listAllChunks(chunks):
	ret = '<div id="chunkHolder">'
	for chunk in chunks:
		ret += '<div id="chunkSummary">' + displayChunkSummary(chunk) + "</div>"

	ret += '</div>'
	return ret



def displayResults(chunks):
	ret = ''
	for chunk in chunks:
		ret += '<div id="chunkSummary">' + displayResultSummary(chunk) + "</div>"
	return ret	

def displayResultSummary(chunk,level = 0):
	retString = ''
	whitespace = '<p>'
	retString += '<h2><a href="/open/' + chunk.id + '">' + chunk.name + '(' + chunk.id + ')</a></h2>'
	retString += whitespace + 'Date:' + str(chunk.date[2]) +'-'+ str(chunk.date[1]) + '-' + str(chunk.date[0]) + '</p>'
	if hasattr(chunk,'parent'):
		retString+='<p>Parent: <a href="/open/' +  chunk.parent.id +  '">' + chunk.parent.name + '</a></p>'
	retString += whitespace + 'Subjects:' + presentSubjectList(chunk.subjects) + '</p>'

	return retString
